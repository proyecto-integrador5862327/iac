variable "name" {
  description = "nombre al recurso"
  type        = string
  default     = "damian"
}

variable "environment" {
  type = string
  default = "dev"
}
